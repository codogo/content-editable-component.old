import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';
import RichText from "../lib/index";

import formatSelection from "../lib/formatSelection";

console.clear();

class CEWithButtons extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			selection: {
				start: 3,
				end: 3,
			},
			//html: "A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.",
			html: "Lorem <u>ipsum</u> dolar",
		};

		this.onChange = this.onChange.bind(this);
	}

	onChange(o){
		//console.log(JSON.stringify(o, null, 2));

		const { selection, html, } = o;

		this.setState({
			selection,
			html,
		});
	}

	render(){
		return(
			<div>
				<RichText
				{ ...this.state }
				onChangeCB = { this.onChange }
				focus = { true }
				/>
				<div>
					<button
						onClick = { () => {
							this.setState(formatSelection(
								this.state,
								{ tag: "u" }
							));
						}}
					>
						Underline
					</button>
					<button
						onClick = { () => {
							this.setState(formatSelection(
								this.state,
								{ tag: "b" }
							));
						}}
					>
						Bold
					</button>
					<button
						onClick = { () => {
							this.setState(formatSelection(
								this.state,
								{ tag: "i" }
							));
						}}
					>
						Italic
					</button>
				</div>
			</div>
		);
	}
}

class FakeStoreComposite extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			selection: {
				start: 3,
				end: 3,
			},
			plain: "This is a thing",
		};

		this.onChange = this.onChange.bind(this);
	}

	onChange(o){
		console.log(JSON.stringify(o, null, 2));

		const { selection, plain, tags, } = o;

		this.setState({
			selection,
			plain,
			tags,
		});
	}

	render(){
		return(
			<RichText
			{ ...this.state }
			onChangeCB = { this.onChange }
			focus = { true }
			/>
		);
	}
};

class FakeStoreHTML extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			selection: {
				start: 3,
				end: 3,
			},
			html: "This is a thing",
		};

		this.onChange = this.onChange.bind(this);
	}

	onChange(o){
		console.log(JSON.stringify(o, null, 2));

		const { selection, html, } = o;

		this.setState({
			selection,
			html,
		});
	}

	render(){
		return(
			<RichText
			{ ...this.state }
			onChangeCB = { this.onChange }
			focus = { true }
			/>
		);
	}
};

storiesOf("content-editable-component", module)
	.add('html input', () => (
		<FakeStoreHTML />
	))
	.add('composite input', () => (
		<FakeStoreComposite />
	))
	.add('with button', () => (
		<CEWithButtons />
	))
;
